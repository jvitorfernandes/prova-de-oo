package view;

import model.*;
import controller.ControllerConsulta;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSlider;
import javax.swing.DropMode;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdicionarConsulta extends JFrame {

	private JPanel contentPane;
	private JTextField tfNomeP;
	private JTextField tfcpfP;
	private JTextField tfIdadeP;
	private JTextField tfSintomas;
	private JTextField tfPrioridade;
	private JTextField tfIdadeM;
	private JTextField tfcpfM;
	private JTextField tfNomeM;
	private JTextField tfCrm;
	private JTextField tfEspecialidade;
	private JTextField tfData;
	private JTextField tfConsultorio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdicionarConsulta frame = new AdicionarConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdicionarConsulta() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 404);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		
		JPanel panel_1 = new JPanel();
		
		JPanel panel_2 = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
						.addComponent(panel_1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 489, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 78, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblDadosDaConsulta = new JLabel("Dados da Consulta");
		
		JLabel lblNewLabel = new JLabel("Data");
		
		JLabel lblConsultrio = new JLabel("Consultório");
		
		tfData = new JTextField();
		tfData.setColumns(10);
		
		tfConsultorio = new JTextField();
		tfConsultorio.setColumns(10);
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//objetos
				ControllerConsulta controla = new ControllerConsulta();
				Paciente umPaciente = new Paciente();
				Medico umMedico = new Medico();
				Consulta umaConsulta = new Consulta();
				
				//recebendo paciente
				String nomeP = tfNomeP.getText();
				String cpfP = tfcpfP.getText();
				int idadeP =  Integer.parseInt(tfIdadeP.getText());
				String sintomas = tfSintomas.getText();
				int prioridade =  Integer.parseInt(tfPrioridade.getText());
				
				umPaciente.setNome(nomeP);
				umPaciente.setSintoma(sintomas);
				umPaciente.setPrioridade(prioridade);
				umPaciente.setCpf(cpfP);
				umPaciente.setIdade(idadeP);
				
				//recebendo médico
				int idadeM =  Integer.parseInt(tfIdadeM.getText());
				String cpfM = tfcpfM.getText();
				String nomeM = tfNomeM.getText();
				String especialidade = tfEspecialidade.getText();
				String crm = tfCrm.getText();
				
				umMedico.setNome(nomeM);
				umMedico.setCrm(crm);
				umMedico.setCpf(cpfM);
				umMedico.setEspecialidade(especialidade);
				umMedico.setIdade(idadeM);
				
				//recebendo consulta
				String data = tfData.getText();
				String consultorio = tfConsultorio.getText();
				
				umaConsulta.setData(data);
				umaConsulta.setConsultorio(consultorio);
				
				umaConsulta.setMedico(umMedico);
				umaConsulta.setPaciente(umPaciente);
				
				controla.adicionarConsulta(umaConsulta);
				
				
				ConsultaAdicionada mensagem = new ConsultaAdicionada();
				mensagem.setVisible(true);
				dispose();
				
				
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfData, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(lblDadosDaConsulta)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblConsultrio)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfConsultorio, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
					.addComponent(btnAdicionar, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
					.addGap(58))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblDadosDaConsulta)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel)
								.addComponent(tfData, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnAdicionar)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblConsultrio)
						.addComponent(tfConsultorio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(41, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		JLabel lblDadosDoMdico = new JLabel("Dados do Médico");
		
		tfIdadeM = new JTextField();
		tfIdadeM.setColumns(10);
		
		JLabel label = new JLabel("Idade");
		
		tfcpfM = new JTextField();
		tfcpfM.setColumns(10);
		
		JLabel label_1 = new JLabel("CPF");
		
		tfNomeM = new JTextField();
		tfNomeM.setColumns(10);
		
		JLabel label_2 = new JLabel("Nome");
		
		JLabel lblCrm = new JLabel("CRM");
		
		JLabel lblEspecialidade = new JLabel("Especialidade");
		
		tfCrm = new JTextField();
		tfCrm.setColumns(10);
		
		tfEspecialidade = new JTextField();
		tfEspecialidade.setColumns(10);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addComponent(lblDadosDoMdico)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(tfNomeM, GroupLayout.PREFERRED_SIZE, 324, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
									.addGap(26)
									.addComponent(tfcpfM, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(label, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
									.addGap(12)
									.addComponent(tfIdadeM, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(lblEspecialidade)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfEspecialidade, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(lblCrm)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfCrm, GroupLayout.PREFERRED_SIZE, 203, GroupLayout.PREFERRED_SIZE)))))
					.addGap(87))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addComponent(lblDadosDoMdico)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(2)
							.addComponent(label_2))
						.addComponent(tfNomeM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(2)
							.addComponent(label_1))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
							.addComponent(tfcpfM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblCrm)
							.addComponent(tfCrm, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(6)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(2)
							.addComponent(label))
						.addComponent(tfIdadeM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblEspecialidade)
							.addComponent(tfEspecialidade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(40, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JLabel lblDadosDoPaciente = new JLabel("Dados do Paciente");
		
		JLabel lblNome = new JLabel("Nome");
		
		JLabel lblCpf = new JLabel("CPF");
		
		JLabel lblIdade = new JLabel("Idade");
		
		JLabel lblSintomas = new JLabel("Sintomas");
		
		JLabel lblPreferncia = new JLabel("Prioridade");
		
		tfNomeP = new JTextField();
		tfNomeP.setColumns(10);
		
		tfcpfP = new JTextField();
		tfcpfP.setColumns(10);
		
		tfIdadeP = new JTextField();
		tfIdadeP.setColumns(10);
		
		tfSintomas = new JTextField();
		tfSintomas.setColumns(10);
		
		tfPrioridade = new JTextField();
		tfPrioridade.setColumns(10);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDadosDoPaciente)
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblIdade)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfIdadeP, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
							.addGap(72)
							.addComponent(lblPreferncia)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfPrioridade, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblSintomas)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfSintomas, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNome)
								.addComponent(lblCpf))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(tfcpfP, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfNomeP, GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE))))
					.addGap(90))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(lblDadosDoPaciente)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(tfNomeP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCpf)
						.addComponent(tfcpfP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIdade)
						.addComponent(lblPreferncia)
						.addComponent(tfIdadeP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfPrioridade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSintomas)
						.addComponent(tfSintomas, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(20, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}
}
