package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JTextArea;
import javax.swing.JLabel;

public class MenuConsulta extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuConsulta frame = new MenuConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuConsulta() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 234, 272);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnAdad = new JButton("Listar Consultas");
		btnAdad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JButton button = new JButton("Adicionar Consulta");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdicionarConsulta adicionar = new AdicionarConsulta();
				adicionar.setVisible(true);
			}
		});
		
		JButton btnCancelarConsulta = new JButton("Cancelar Consulta");
		
		JButton btnAdicionarDiagnstico = new JButton("Adicionar Diagnóstico");
		
		JButton btnVerDiagnstico = new JButton("Ver diagnóstico");
		
		JLabel lblMenu = new JLabel("MENU");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
									.addComponent(button, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
									.addComponent(btnAdad)
									.addComponent(btnCancelarConsulta, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addComponent(btnAdicionarDiagnstico)
								.addComponent(btnVerDiagnstico)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(82)
							.addComponent(lblMenu)))
					.addContainerGap(390, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblMenu)
					.addGap(7)
					.addComponent(button)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnAdad)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCancelarConsulta)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnAdicionarDiagnstico)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnVerDiagnstico)
					.addContainerGap(229, Short.MAX_VALUE))
		);
		gl_contentPane.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnAdad, button, btnCancelarConsulta, btnAdicionarDiagnstico, btnVerDiagnstico});
		contentPane.setLayout(gl_contentPane);
	}
}
