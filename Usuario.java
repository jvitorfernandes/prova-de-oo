public class Usuario{
	
	private String nome;
	private int idade;
	private String cpf;
	
	public void setNome(String umNome){
		this.nome = umNome;		
	}

	public String getNome(){
		return nome;		
	}

	public void setIdade(int umaIdade){
		this.idade = umaIdade;		
	}
	
	public int getIdade(){
		return idade;		
	}

	public void setCpf(String umCpf){
		this.cpf = umCpf;		
	}
	public String getCpf(){
		return cpf;		
	}
}
