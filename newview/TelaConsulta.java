package newview;

import model.*;


import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import controller.ControllerConsulta;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
public class TelaConsulta {

	private JFrame frame;
	private JTextField tfNomeP;
	private JTextField tfCpfP;
	private JTextField tfIdadeP;
	private JTextField tfPrioridade;
	private JTextField tfSintomas;
	private JTextField tfNomeM;
	private JTextField tfCpfM;
	private JTextField tfIdadeM;
	private JTextField tfCrm;
	private JTextField tfData;
	private JTextField tfConsultorio;
	private JTextField tfEspecialidade;
	
	//private JTextArea textArea;
	
	//private JPanel menuPanel;
	//private JPanel adicionarPanel;
	//private JPanel listarPanel;
	//private JPanel cancelarPanel;
	
	ControllerConsulta controla = new ControllerConsulta();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaConsulta window = new TelaConsulta();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TelaConsulta() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 339);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		final JPanel menuPanel = new JPanel();
		frame.getContentPane().add(menuPanel, "name_547342016275");
		menuPanel.setLayout(null);
		menuPanel.setVisible(true);
		
		final JPanel adicionarPanel = new JPanel();
		frame.getContentPane().add(adicionarPanel, "name_549034103594");
		adicionarPanel.setLayout(null);
		adicionarPanel.setVisible(false);
		
		final JPanel listarPanel = new JPanel();
		frame.getContentPane().add(listarPanel, "name_551046433426");
		listarPanel.setLayout(null);
		listarPanel.setVisible(false);
		
		final JPanel cancelarPanel = new JPanel();
		frame.getContentPane().add(cancelarPanel, "name_10419515048038");
		cancelarPanel.setLayout(null);
		cancelarPanel.setVisible(false);
		
		
		JLabel lblListaDeConsultas = new JLabel("Lista de Consultas Marcadas");
		lblListaDeConsultas.setBounds(12, 12, 256, 15);
		listarPanel.add(lblListaDeConsultas);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 39, 278, 260);
		listarPanel.add(scrollPane);
		
		final JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		
		JButton btnAddConsulta = new JButton("Adicionar Consuta");
		btnAddConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				adicionarPanel.setVisible(true);
				menuPanel.setVisible(false);
				
			}
		});
		btnAddConsulta.setBounds(35, 69, 214, 25);
		menuPanel.add(btnAddConsulta);
		
		JButton btnListarConsultas = new JButton("Listar Consultas");
		btnListarConsultas.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				menuPanel.setVisible(false);
				listarPanel.setVisible(true);
				textArea.setText("");
				controla.listarConsulta(textArea);
				
			}
		});
		btnListarConsultas.setBounds(35, 105, 214, 25);
		menuPanel.add(btnListarConsultas);
		
		JButton btnNewButton = new JButton("Cancelar Consulta");
		btnNewButton.setBounds(35, 142, 214, 25);
		menuPanel.add(btnNewButton);
		
		
		//ADICIONAR
		//JPanel adicionarPanel = new JPanel();
		frame.getContentPane().add(adicionarPanel, "name_549034103594");
		
		JLabel lblAdicionarConsulta = new JLabel("Adicionar Consulta");
		
		JLabel lblNome = new JLabel("Nome");
		
		JLabel lblNewLabel = new JLabel("CPF");
		
		JLabel lblNewLabel_1 = new JLabel("Idade");
		
		tfNomeP = new JTextField();
		tfNomeP.setColumns(10);
		
		tfCpfP = new JTextField();
		tfCpfP.setColumns(10);
		
		tfIdadeP = new JTextField();
		tfIdadeP.setColumns(10);
		
		JLabel lblPreferncia = new JLabel("Prioridade");
		
		tfPrioridade = new JTextField();
		tfPrioridade.setColumns(10);
		
		JLabel lblSintomas = new JLabel("Sintomas");
		
		tfSintomas = new JTextField();
		tfSintomas.setColumns(10);
		
		JSeparator separator = new JSeparator();
		
		JLabel lblMdico = new JLabel("Médico");
		
		tfNomeM = new JTextField();
		tfNomeM.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF");
		
		JLabel lblIdade = new JLabel("Idade");
		
		tfCpfM = new JTextField();
		tfCpfM.setColumns(10);
		
		tfIdadeM = new JTextField();
		tfIdadeM.setColumns(10);
		
		JLabel lblCrm = new JLabel("CRM");
		
		tfCrm = new JTextField();
		tfCrm.setColumns(10);
		
		JSeparator separator_1 = new JSeparator();
		
		JLabel lblData = new JLabel("Data");
		
		tfData = new JTextField();
		tfData.setColumns(10);
		
		JLabel lblConsultrio = new JLabel("Consultório");
		
		tfConsultorio = new JTextField();
		tfConsultorio.setColumns(10);
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Paciente umPaciente = new Paciente();
				Medico umMedico = new Medico();
				Consulta umaConsulta = new Consulta();
				
				//int cont=0;
				
				//recebendo paciente
				String nomeP = tfNomeP.getText();
				String cpfP = tfCpfP.getText();
				int idadeP =  Integer.parseInt(tfIdadeP.getText());
				String sintomas = tfSintomas.getText();
				int prioridade =  Integer.parseInt(tfPrioridade.getText());
				
				umPaciente.setNome(nomeP);
				umPaciente.setSintoma(sintomas);
				umPaciente.setPrioridade(prioridade);
				umPaciente.setCpf(cpfP);
				umPaciente.setIdade(idadeP);
				
				//recebendo médico
				int idadeM =  Integer.parseInt(tfIdadeM.getText());
				String cpfM = tfCpfM.getText();
				String nomeM = tfNomeM.getText();
				String especialidade = tfEspecialidade.getText();
				String crm = tfCrm.getText();
				
				umMedico.setNome(nomeM);
				umMedico.setCrm(crm);
				umMedico.setCpf(cpfM);
				umMedico.setEspecialidade(especialidade);
				umMedico.setIdade(idadeM);
				
				//recebendo consulta
				String data = tfData.getText();
				String consultorio = tfConsultorio.getText();
				
				umaConsulta.setData(data);
				umaConsulta.setConsultorio(consultorio);
				
				umaConsulta.setMedico(umMedico);
				umaConsulta.setPaciente(umPaciente);
				
				controla.adicionarConsulta(umaConsulta);
				
				
				adicionarPanel.setVisible(false);
				menuPanel.setVisible(true);
				
				Adicionado mensagem = new Adicionado();
				mensagem.setVisible(true);
				
				//controla.listarConsulta();
				
			}
		});
		
		JSeparator separator_2 = new JSeparator();
		
		JLabel lblEspecialidade = new JLabel("Especialidade");
		
		tfEspecialidade = new JTextField();
		tfEspecialidade.setColumns(10);
		GroupLayout gl_adicionarPanel = new GroupLayout(adicionarPanel);
		gl_adicionarPanel.setHorizontalGroup(
			gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_adicionarPanel.createSequentialGroup()
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_adicionarPanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addComponent(lblData)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfData, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addComponent(lblConsultrio)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfConsultorio, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)))
							.addGap(18)
							.addComponent(btnAdicionar))
						.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addContainerGap()
									.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addGap(141)
									.addComponent(lblAdicionarConsulta, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addGap(12)
									.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNome, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_adicionarPanel.createSequentialGroup()
											.addGap(59)
											.addComponent(tfNomeP, GroupLayout.PREFERRED_SIZE, 348, GroupLayout.PREFERRED_SIZE))))
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addGap(12)
									.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_adicionarPanel.createSequentialGroup()
											.addGap(59)
											.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
												.addComponent(tfIdadeP, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
												.addComponent(tfCpfP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
										.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
									.addGap(7)
									.addComponent(lblPreferncia)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfPrioridade, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addContainerGap()
									.addComponent(lblSintomas)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfSintomas, 329, 329, 329))
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addContainerGap()
									.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblMdico)
										.addComponent(lblCpf))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(tfNomeM, 345, 345, 345)
										.addGroup(gl_adicionarPanel.createSequentialGroup()
											.addComponent(tfCpfM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(lblCrm)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(tfCrm, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_adicionarPanel.createSequentialGroup()
											.addComponent(tfIdadeM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(lblEspecialidade)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(tfEspecialidade, GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE))))
								.addGroup(gl_adicionarPanel.createSequentialGroup()
									.addContainerGap()
									.addComponent(lblIdade)))
							.addGroup(gl_adicionarPanel.createSequentialGroup()
								.addContainerGap()
								.addComponent(separator_1, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE))))
					.addGap(31))
		);
		gl_adicionarPanel.setVerticalGroup(
			gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_adicionarPanel.createSequentialGroup()
					.addGap(12)
					.addComponent(lblAdicionarConsulta)
					.addGap(10)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_adicionarPanel.createSequentialGroup()
							.addGap(2)
							.addComponent(lblNome))
						.addComponent(tfNomeP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(1)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_adicionarPanel.createSequentialGroup()
							.addGap(2)
							.addComponent(lblNewLabel))
						.addComponent(tfCpfP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(2)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(tfIdadeP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPreferncia)
						.addComponent(tfPrioridade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSintomas)
						.addComponent(tfSintomas, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(separator, GroupLayout.PREFERRED_SIZE, 4, GroupLayout.PREFERRED_SIZE)
						.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, 3, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(tfNomeM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblMdico))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCpf)
						.addComponent(tfCpfM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCrm)
						.addComponent(tfCrm, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblIdade)
							.addComponent(tfIdadeM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblEspecialidade)
							.addComponent(tfEspecialidade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(separator_1, GroupLayout.PREFERRED_SIZE, 3, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_adicionarPanel.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblData)
								.addComponent(tfData, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_adicionarPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblConsultrio)
								.addComponent(tfConsultorio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_adicionarPanel.createSequentialGroup()
							.addGap(16)
							.addComponent(btnAdicionar)))
					.addContainerGap(245, Short.MAX_VALUE))
		);
		adicionarPanel.setLayout(gl_adicionarPanel);
		
		//JPanel listarPanel = new JPanel();
		//frame.getContentPane().add(listarPanel, "name_551046433426");
		
		JButton btnNewButton_1 = new JButton("Voltar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				menuPanel.setVisible(true);
				listarPanel.setVisible(false);
			}
		});
		btnNewButton_1.setBounds(312, 237, 117, 25);
		listarPanel.add(btnNewButton_1);
		
		
		//Cancelar
		JLabel lblNmeroDeCadastro = new JLabel("Número de Cadastro da Consulta");
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JLabel lblCancelarConsulta = new JLabel("Cancelar Consulta");
		
		JButton btnCancelarConsulta = new JButton("Cancelar Consulta");
		GroupLayout gl_cancelarPanel = new GroupLayout(cancelarPanel);
		gl_cancelarPanel.setHorizontalGroup(
			gl_cancelarPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_cancelarPanel.createSequentialGroup()
					.addGroup(gl_cancelarPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_cancelarPanel.createSequentialGroup()
							.addGap(161)
							.addComponent(lblCancelarConsulta))
						.addGroup(gl_cancelarPanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_cancelarPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnCancelarConsulta)
								.addGroup(gl_cancelarPanel.createSequentialGroup()
									.addComponent(lblNmeroDeCadastro)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(textField, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))))
					.addContainerGap(126, Short.MAX_VALUE))
		);
		gl_cancelarPanel.setVerticalGroup(
			gl_cancelarPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_cancelarPanel.createSequentialGroup()
					.addGap(6)
					.addComponent(lblCancelarConsulta)
					.addGap(18)
					.addGroup(gl_cancelarPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNmeroDeCadastro)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnCancelarConsulta)
					.addContainerGap(216, Short.MAX_VALUE))
		);
		cancelarPanel.setLayout(gl_cancelarPanel);
		
		
	}
}
