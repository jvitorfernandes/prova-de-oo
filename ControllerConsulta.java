import java.util.*;
public class ControllerConsulta{

	ArrayList<Consulta> listaConsultas = new ArrayList<Consulta>();
	

	public void adicionarConsulta(Consulta umaConsulta){
		listaConsultas.add(umaConsulta);
	}

	public void cancelarConsulta(int x){
		listaConsultas.remove(x);
	}

	public void listarConsulta(){
		for(int i=0; i<listaConsultas.size(); i++){
		
			System.out.println("Data: " + listaConsultas.get(i).getData());
			System.out.println("Consultório: " + listaConsultas.get(i).getConsultorio());
			System.out.println("Médico: " + listaConsultas.get(i).getMedico().getNome());
			System.out.println("Registro CRM: " + listaConsultas.get(i).getMedico().getCrm());
			System.out.println("Paciente: " + listaConsultas.get(i).getPaciente().getNome());
			System.out.println("Sintoma(s): " + listaConsultas.get(i).getPaciente().getSintoma());
			System.out.println("Prioridade: " + listaConsultas.get(i).getPaciente().getPrioridade());
		}
	}
	
	public void verDiagnostico(int aux){
	
		System.out.println("Diagnóstico: " + listaConsultas.get(aux).getDiagnostico());
	}
}
