public class Consulta{

	Paciente umPaciente;	
	Medico umMedico;
	private String data;
	private String consultorio;
	private String diagnostico;
	
	

	public void setData(String umaData){
		this.data = umaData;		
	}

	public String getData(){
		return data;		
	}

	public void setConsultorio(String umConsultorio){
		this.consultorio = umConsultorio;		
	}

	public String getConsultorio(){
		return consultorio;		
	}
	public void setMedico(Medico umMedico){
		this.umMedico = umMedico;
	}
	
	public Medico getMedico(){
		return umMedico;
	}
	
	public void setPaciente(Paciente umPaciente){
		this.umPaciente = umPaciente;
	}
	
	public Paciente getPaciente(){
		return umPaciente;
	}

	public void setDiagnostico(String umDiagnostico){
		this.diagnostico = umDiagnostico;
	}
	
	public String getDiagnostico(){
		return diagnostico;
	}


}
