public class Medico extends Usuario{

	private String crm;
	private String especialidade;
	

	public void setCrm(String umCrm){
		this.crm = umCrm;		
	}

	public String getCrm(){
		return crm;		
	}

	public void setEspecialidade(String umaEspecialidade){
		this.especialidade = umaEspecialidade;		
	}

	public String getEspecialidade(){
		return especialidade;		
	}
}
