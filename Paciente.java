public class Paciente extends Usuario{

	private int prioridade;
	private String sintoma;
	

	public void setPrioridade(int umaPrioridade){
		this.prioridade = umaPrioridade;		
	}

	public int getPrioridade(){
		return prioridade;		
	}

	public void setSintoma(String umSintoma){
		this.sintoma = umSintoma;		
	}

	public String getSintoma(){
		return sintoma;		
	}
}
