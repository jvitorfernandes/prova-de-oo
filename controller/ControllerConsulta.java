
package controller;

import model.*;

import java.util.*;

import javax.swing.JTextArea;
public class ControllerConsulta{

	public ArrayList<Consulta> listaConsultas = new ArrayList<Consulta>();
	
	
	
	
	public void adicionarConsulta(Consulta umaConsulta){
		listaConsultas.add(umaConsulta);
	}

	public void cancelarConsulta(int x){
		listaConsultas.remove(x);
	}

	public void listarConsulta(JTextArea textArea){
		
		for(int i=0; i<listaConsultas.size(); i++){
			
			textArea.append("#" +(i+1)+ ":");
			textArea.append("\tData: " + listaConsultas.get(i).getData());
			textArea.append("\n\tConsultório: " + listaConsultas.get(i).getConsultorio());
			textArea.append("\n\tMédico: " + listaConsultas.get(i).getMedico().getNome());
			textArea.append("\n\tRegistro CRM: " + listaConsultas.get(i).getMedico().getCrm());
			textArea.append("\n\tPaciente: " + listaConsultas.get(i).getPaciente().getNome());
			textArea.append("\n\tSintoma(s): " + listaConsultas.get(i).getPaciente().getSintoma());
			textArea.append("\n\tPrioridade: " + listaConsultas.get(i).getPaciente().getPrioridade()+"\n\n");
		}
	}
	
	public void addDiagnostico(int n, String umDiagnostico){
		
		
		listaConsultas.get(n).setDiagnostico(umDiagnostico);
		
	}
	
	public void verDiagnostico(int i){
		
		System.out.println("#" +(i+1)+ ":");
		System.out.println("Data: " + listaConsultas.get(i).getData());
		System.out.println("Consultório: " + listaConsultas.get(i).getConsultorio());
		System.out.println("Médico: " + listaConsultas.get(i).getMedico().getNome());
		System.out.println("Registro CRM: " + listaConsultas.get(i).getMedico().getCrm());
		System.out.println("Paciente: " + listaConsultas.get(i).getPaciente().getNome());
		System.out.println("Sintoma(s): " + listaConsultas.get(i).getPaciente().getSintoma());
		System.out.println("Prioridade: " + listaConsultas.get(i).getPaciente().getPrioridade());
		System.out.println("Diagnóstico: " + listaConsultas.get(i).getDiagnostico());
	}
}
